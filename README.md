# README

Ce dépôt sert de base pour la réalisation du workshop 2 Git de CTLSSG

## Contenu

Cet atelier va porter sur:

* le staging
* les commits
* la modification de l'historique

Il ne traitera que ces sujets.

PATTERN1


## Exercices

2 exercices sont présents dans cet atelier, le premier porte sur le staging, le second sur le rebase interractif.


## Informations générales

* Durée: ~1h15
* Niveau requis: connaissances des concepts Git de base ainsi que des principales commandes.
* Prérequis: 
    * installation de GitBash
    * Compte BitBucket avec accès aux projets du compte ctlssg

## Todos

PATTERN2

