# README

Ce dépôt sert de base pour la réalisation du workshop 2 Git de CTLSSG

## Contenu

Cet atelier va porter sur:

* le staging
* les commits
* la modification de l'historique

Il ne traitera que ces sujets.

## Exercices

2 exercices sont présents dans cet atelier, le premier porte sur le staging, le second sur le rebase interractif.


## Informations générales

* Durée: ~1h15
* Niveau requis: connaissances des concepts Git de base ainsi que des principales commandes.
* Prérequis: 
    * installation de GitBash
    * Compte BitBucket avec accès aux projets du compte ctlssg

## Todos



Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut vel purus leo. Nulla magna lorem, varius lacinia magna ut, vehicula scelerisque tortor. Donec ac magna vel diam euismod sollicitudin at eget ligula. Duis sed egestas nisi. Fusce commodo, nibh a euismod pulvinar, arcu enim pretium lectus, quis aliquam lacus lacus tincidunt enim. Sed varius ante mattis orci consequat, vel iaculis ante dapibus. Fusce pretium egestas iaculis. Etiam nulla urna, convallis consectetur blandit elementum, rhoncus nec odio. Praesent eget condimentum metus. Quisque convallis pretium sapien vitae porttitor. Sed et pharetra nulla. Etiam consectetur sodales nisi, ac mollis enim facilisis ut. Nam pulvinar hendrerit ipsum, id blandit urna fermentum vel. Fusce dictum sapien elit, eget malesuada eros pulvinar ac. Praesent molestie in arcu id pharetra. Maecenas ultrices, turpis non dignissim mattis, neque odio faucibus risus, vitae finibus ante orci in felis.

Sed sit amet maximus odio. Maecenas pharetra commodo orci quis eleifend. Morbi egestas ex in pretium volutpat. Phasellus ac ante sed nulla tempor ultrices euismod non felis. Integer risus libero, cursus imperdiet sollicitudin ut, iaculis varius mi. Aliquam vel luctus eros. Vestibulum placerat ante est, at mollis nisl facilisis a. Phasellus lobortis ante eget imperdiet pulvinar. Pellentesque vel rutrum metus. Nunc quis dignissim nunc. Nulla non eros augue. Sed congue lacus vel vestibulum laoreet. Sed eleifend ornare ante, id ultrices purus pulvinar a. Vestibulum ex ante, varius sit amet neque vel, euismod accumsan felis.

Interdum et malesuada fames ac ante ipsum primis in faucibus. In elementum commodo lacus ut ornare. Suspendisse eros urna, malesuada eu consectetur et, semper eget mauris. Curabitur vel massa at est scelerisque bibendum. Etiam tempus blandit leo, ut consequat nibh consequat in. Aenean rutrum massa sapien, eget lobortis neque mattis eu. Proin interdum luctus ligula eu viverra. Nunc sit amet libero at justo hendrerit imperdiet in id lectus. Suspendisse ullamcorper sollicitudin dui cursus accumsan. In laoreet sem vel risus condimentum, ac ultrices leo tempor. Nam sit amet nunc in justo maximus tincidunt eget eget quam.

Pellentesque non sapien et odio sollicitudin tempus ac non urna. Quisque auctor neque eu justo sollicitudin, ac sollicitudin lectus ullamcorper. Proin ultricies fringilla dui id sagittis. In tempus purus at rutrum malesuada. Duis et enim mauris. Integer gravida tempor risus. Nullam lacinia velit massa, sed porta urna tempus congue. In ullamcorper dui id lacus vehicula elementum.

Curabitur lobortis lectus a massa rhoncus, quis congue diam pretium. Phasellus quis eros mollis, tristique ligula egestas, rhoncus tortor. In in posuere enim. Ut non congue lacus. Maecenas aliquam tellus eget euismod semper. Cras auctor magna at elit porta tincidunt. Nullam venenatis nisl vitae mi iaculis, et eleifend justo elementum. Aenean ut vulputate dui. Sed gravida fringilla nisl in semper. Phasellus massa odio, vehicula sed nunc at, lobortis porta nulla. Nulla vitae nisl placerat, feugiat mauris tempus, consequat felis. Donec eu elit eros. Proin ullamcorper nisl a elit aliquet, a semper tellus consequat. Cras mollis mollis lorem at porttitor. Curabitur in turpis porta, tincidunt turpis id, dapibus sapien. Donec semper turpis sapien, ac laoreet magna egestas in. 

