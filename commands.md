# Git wokshop 2 - Liste des commandes

## Commandes

### Slide #/2/3

    luc%  git status
    On branch master
    Changes to be committed:
      (use "git reset HEAD &lt;file&gt;..." to unstage)
            modified:   README.md

    Changes not staged for commit:
       (use "git add &lt;file&gt;..." to update what will be committed)
       (use "git checkout -- &lt;file&gt;..." to discard changes in working directory)
            modified:   README.md

    Untracked files:
      (use "git add &lt;file&gt;..." to include in what will be committed)
            untracked.txt

### Slide #/2/5

    luc% git add README.md 	# ajout d'un fichier dans le stage 

    luc% git add --all 	# ajout de tous les fichiers modifiés 
			# ou non trackés dans le stage

### Slide #/2/6

    luc% git rm DEL.txt 
    rm 'DEL.txt'

    git status
    On branch master
    Changes to be committed:
      (use "git reset HEAD &lt;file&gt;..." to unstage)

            deleted:    DEL.txt

    Untracked files:
      (use "git add &lt;file&gt;..." to include in what will be committed)

            untracked.txt

### Slide #/2/7

    git mv MV.txt MOVE.txt

    git status
    On branch master
    Changes to be committed:
      (use "git reset HEAD &lt;file&gt;..." to unstage)

            renamed:    MV.txt -> MOVE.txt

    Untracked files:
      (use "git add &lt;file&gt;..." to include in what will be committed)

            untracked.txt
								
### Slide #/2/8

    git add -i README.md

### Slide #/3/1

    luc% git commit --amend
    [master 3a5881b] split commit 2
     Date: Fri Jun 3 17:31:16 2016 +0200
     1 file changed, 1 insertion(+)

### Slide #/3/4

    luc%  git rebase -i HEAD~2
    Successfully rebased and updated refs/heads/master.

    # Fichier de configuration du rebase à modifier afin d'obtenir le résultat voulu:
    pick 7617da2 Ajout fichier MV.txt
    reword 2fd665f Ajout de Move.txt au dépôt
    # Rebase a6fd281..2fd665f onto a6fd281 (2 command(s))
    # Commands:
    # p, pick = use commit
    # r, reword = use commit, but edit the commit message
    # e, edit = use commit, but stop for amending
    # s, squash = use commit, but meld into previous commit
    # f, fixup = like "squash", but discard this commit's log message
    # x, exec = run command (the rest of the line) using shell
    # d, drop = remove commit
    # These lines can be re-ordered; they are executed from top to bottom.
    # If you remove a line here THAT COMMIT WILL BE LOST.
    # However, if you remove everything, the rebase will be aborted.
    # Note that empty commits are commented out

### Slide #/3/5

    luc%  git rebase -i HEAD~1
    Successfully rebased and updated refs/heads/master.

    # Fichier de configuration du rebase à modifier afin d'obtenir le résultat voulu:
    drop 3c0b008 demo
    # Rebase e7c812d..3c0b008 onto e7c812d (1 command(s))
    # Commands:
    # p, pick = use commit
    # r, reword = use commit, but edit the commit message
    # e, edit = use commit, but stop for amending
    # s, squash = use commit, but meld into previous commit
    # f, fixup = like "squash", but discard this commit's log message
    # x, exec = run command (the rest of the line) using shell
    # d, drop = remove commit
    # These lines can be re-ordered; they are executed from top to bottom.
    # If you remove a line here THAT COMMIT WILL BE LOST.
    # However, if you remove everything, the rebase will be aborted.
    # Note that empty commits are commented out

### Slide #/3/6

    luc% git tree
    * 3c0b008 (HEAD -> master) demo
    * e7c812d Ajout de MOVE.txt au dépôt
    * 7617da2 Ajout fichier MV.txt
    * a6fd281 Ajout fichier DEL.txt
    * 48f5729 Part 2 - git add
    * bc9d819 Part 1 - git add
    * 62cc0bb add comments
    * 4ce9bd4 Ajout de commentaires
    | * dcbf4d0 (sw-ri) Ajout de commentaires (rebase interactif...)
    |/
    * c219493 README Partial commit part 2
    * 823fb77 README Partial commit part 1
    * 531f4aa Commit initial

    luc% git tree
    * e7c812d (HEAD -> master) Ajout de MOVE.txt au dépôt
    * 7617da2 Ajout fichier MV.txt
    * a6fd281 Ajout fichier DEL.txt
    * 48f5729 Part 2 - git add
    * bc9d819 Part 1 - git add
    * 62cc0bb add comments
    * 4ce9bd4 Ajout de commentaires
    | * dcbf4d0 (sw-ri) Ajout de commentaires (rebase interactif...)
    |/
    * c219493 README Partial commit part 2
    * 823fb77 README Partial commit part 1
    * 531f4aa Commit initial

### Slide #/3/7

    luc% git rebase -i HEAD~2
    Successfully rebased and updated refs/heads/master.

    # Fichier de configuration du rebase à modifier afin d'obtenir le résultat voulu:
    pick c892e02 ajout autres infos dans SORTING
    pick d7a10ed ajout sorting bis
    # Rebase 3c26119..c892e02 onto 3c26119 (2 command(s))
    # Commands:
    # p, pick = use commit
    # r, reword = use commit, but edit the commit message
    # e, edit = use commit, but stop for amending
    # s, squash = use commit, but meld into previous commit
    # f, fixup = like "squash", but discard this commit's log message
    # x, exec = run command (the rest of the line) using shell
    # d, drop = remove commit
    # These lines can be re-ordered; they are executed from top to bottom.
    # If you remove a line here THAT COMMIT WILL BE LOST.
    # However, if you remove everything, the rebase will be aborted.
    # Note that empty commits are commented out

### Slide #/3/8

    luc% git tree
    * d7a10ed (HEAD -> master) ajout sorting bis
    * c892e02 ajout autres infos dans SORTING
    * 96b1da7 ajout info dans SORTING
    * 3c26119 Ajout SORTING
    * e7c812d Ajout de MOVE.txt au dépôt
    * 7617da2 Ajout fichier MV.txt
    * a6fd281 Ajout fichier DEL.txt
    * 48f5729 Part 2 - git add
    * bc9d819 Part 1 - git add
    * 62cc0bb add comments
    * 4ce9bd4 Ajout de commentaires
    | * dcbf4d0 (sw-ri) Ajout de commentaires (rebase interactif...)
    |/
    * c219493 README Partial commit part 2
    * 823fb77 README Partial commit part 1
    * 531f4aa Commit initial

    luc% git tree
    * aa08059 (HEAD -> master) ajout autres infos dans SORTING
    * c8eebd2 ajout sorting bis
    * 96b1da7 ajout info dans SORTING
    * 3c26119 Ajout SORTING
    * e7c812d Ajout de MOVE.txt au dépôt
    * 7617da2 Ajout fichier MV.txt
    * a6fd281 Ajout fichier DEL.txt
    * 48f5729 Part 2 - git add
    * bc9d819 Part 1 - git add
    * 62cc0bb add comments
    * 4ce9bd4 Ajout de commentaires
    | * dcbf4d0 (sw-ri) Ajout de commentaires (rebase interactif...)
    |/
    * c219493 README Partial commit part 2
    * 823fb77 README Partial commit part 1
    * 531f4aa Commit initial

### Slide #/3/9

    luc% git rebase -i HEAD~3
    [detached HEAD 19fe545] ajout info dans SORTING
     Date: Fri Jun 3 15:28:23 2016 +0200
     1 file changed, 2 insertions(+)
    Successfully rebased and updated refs/heads/master.

    # Fichier de configuration du rebase à modifier afin d'obtenir le résultat voulu:
    pick 96b1da7 ajout info dans SORTING
    fixup 605d894 ajout autres infos dans SORTING
    pick ead95c0 ajout sorting bis
    # Rebase 3c26119..ead95c0 onto 3c26119 (3 command(s))
    #
    # ...
    #
    # These lines can be re-ordered; they are executed from top to bottom.
    # If you remove a line here THAT COMMIT WILL BE LOST.
    # However, if you remove everything, the rebase will be aborted.
    # Note that empty commits are commented out

### Slide #/3/10

    luc% git tree
    * ead95c0 (HEAD -> master) ajout sorting bis
    * 605d894 ajout autres infos dans SORTING
    * 96b1da7 ajout info dans SORTING
    * 3c26119 Ajout SORTING
    * e7c812d Ajout de MOVE.txt au dépôt
    * 7617da2 Ajout fichier MV.txt
    * a6fd281 Ajout fichier DEL.txt
    * 48f5729 Part 2 - git add
    * bc9d819 Part 1 - git add
    * 62cc0bb add comments
    * 4ce9bd4 Ajout de commentaires
    | * dcbf4d0 (sw-ri) Ajout de commentaires (rebase interactif...)
    |/
    * c219493 README Partial commit part 2
    * 823fb77 README Partial commit part 1
    * 531f4aa Commit initial

    luc% git tree
    * 9e64c60 (HEAD -> master) ajout sorting bis
    * 19fe545 ajout info dans SORTING
    * 3c26119 Ajout SORTING
    * e7c812d Ajout de MOVE.txt au dépôt
    * 7617da2 Ajout fichier MV.txt
    * a6fd281 Ajout fichier DEL.txt
    * 48f5729 Part 2 - git add
    * bc9d819 Part 1 - git add
    * 62cc0bb add comments
    * 4ce9bd4 Ajout de commentaires
    | * dcbf4d0 (sw-ri) Ajout de commentaires (rebase interactif...)
    |/
    * c219493 README Partial commit part 2
    * 823fb77 README Partial commit part 1
    * 531f4aa Commit initial

### Slide #/3/11

    luc% git rebase -i HEAD~1
    Stopped at c8279bbb5164c1f969c1bc4281bc14ca8bc198d5... Commit à spliter
    You can amend the commit now, with
            git commit --amend
    Once you are satisfied with your changes, run
            git rebase --continue

    # Fichier de configuration du rebase à modifier afin d'obtenir le résultat voulu:
    edit c8279bb Commit à spliter
    # Rebase 064468f..c8279bb onto 064468f (1 command(s))
    # 
    # ...
    # 
    # These lines can be re-ordered; they are executed from top to bottom.
    # If you remove a line here THAT COMMIT WILL BE LOST.
    # However, if you remove everything, the rebase will be aborted.
    # Note that empty commits are commented out

### Slide #/3/12

    luc% git reset -p HEAD^
    diff --git b/split.txt a/split.txt
    index d0c3ba0..d90cd3c 100644
    --- b/split.txt
    +++ a/split.txt
    @@ -2,7 +2,6 @@

     Partie 1 du commit à mettre dans le commit 1
     Partie 2 du commit à mettre dans le commit 2
    -**Ceci est la partie une de la documentation ne servant à rien de cet atelier**
     Partie 1 du commit à mettre dans le commit 1
     Partie 2 du commit à mettre dans le commit 2
     Partie 1 du commit à mettre dans le commit 1
    Apply this hunk to index [y,n,q,a,d,/,j,J,g,e,?]? y
    @@ -12,8 +11,8 @@ Partie 2 du commit à mettre dans le commit 2

     Partie 1 du commit à mettre dans le commit 1
     Partie 2 du commit à mettre dans le commit 2
    -Ceci est la partie deux de la documentation ne servant pas plus que la partie
    -une pour cet atelier
    +Partie 1 du commit à mettre dans le commit 1
    +Partie 2 du commit à mettre dans le commit 2
     Partie 1 du commit à mettre dans le commit 1
     Partie 2 du commit à mettre dans le commit 2

    Apply this hunk to index [y,n,q,a,d,/,K,g,e,?]? n

### Slide #/3/13

    luc% git commit --amend
    [detached HEAD daeca61] split commit 1
     Date: Fri Jun 3 17:27:10 2016 +0200
     1 file changed, 2 insertions(+), 2 deletions(-)

    luc% git add split.txt && git commit -m "split commit 2"
    [detached HEAD c43003c] split commit 2
     1 file changed, 1 insertion(+)

    luc% git rebase --continue
    Successfully rebased and updated refs/heads/master.


### Slide #3/14

    luc% git tree
    * c8279bb (HEAD -> master) Commit à spliter
    * 064468f ajout du fichier split pour apptéhension du split de commit
    * 9e64c60 ajout sorting bis
    * 19fe545 ajout info dans SORTING
    * 3c26119 Ajout SORTING
    * e7c812d Ajout de MOVE.txt au dépôt
    * 7617da2 Ajout fichier MV.txt
    * a6fd281 Ajout fichier DEL.txt
    * 48f5729 Part 2 - git add
    * bc9d819 Part 1 - git add
    * 62cc0bb add comments
    * 4ce9bd4 Ajout de commentaires
    | * dcbf4d0 (sw-ri) Ajout de commentaires (rebase interactif...)
    |/
    * c219493 README Partial commit part 2
    * 823fb77 README Partial commit part 1
    * 531f4aa Commit initial

    luc% git tree
    * c43003c (HEAD -> master) split commit 2
    * daeca61 split commit 1
    * 064468f ajout du fichier split pour apptéhension du split de commit
    * 9e64c60 ajout sorting bis
    * 19fe545 ajout info dans SORTING
    * 3c26119 Ajout SORTING
    * e7c812d Ajout de MOVE.txt au dépôt
    * 7617da2 Ajout fichier MV.txt
    * a6fd281 Ajout fichier DEL.txt
    * 48f5729 Part 2 - git add
    * bc9d819 Part 1 - git add
    * 62cc0bb add comments
    * 4ce9bd4 Ajout de commentaires
    | * dcbf4d0 (sw-ri) Ajout de commentaires (rebase interactif...)
    |/
    * c219493 README Partial commit part 2
    * 823fb77 README Partial commit part 1
    * 531f4aa Commit initial

### Slide #/3/15

    luc% git tree	
