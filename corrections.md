# Correction Exercices

## Exercice staging

    luc% git rm deprecated.txt
    rm 'deprecated.txt'

    luc% git mv foo.txt bar.txt

    luc% vi marchaud-luc.txt

    luc% git add marchaud-luc.txt
    warning: LF will be replaced by CRLF in marchaud-luc.txt.
    The file will have its original line endings in your working directory.

    luc% git commit -m "part 1 exercice"
    [sw-exercice-staging 27818a9] part 1 exercice
    warning: LF will be replaced by CRLF in marchaud-luc.txt.
    The file will have its original line endings in your working directory.
     3 files changed, 1 insertion(+), 41 deletions(-)
     rename foo.txt => bar.txt (100%)
     delete mode 100644 deprecated.txt
     create mode 100644 marchaud-luc.txt

    luc% ./exercice_staging.sh

    luc% git status
    On branch sw-exercice-staging
    Your branch is ahead of 'origin/sw-exercice-staging' by 1 commit.
      (use "git push" to publish your local commits)
    Changes not staged for commit:
      (use "git add <file>..." to update what will be committed)
      (use "git checkout -- <file>..." to discard changes in working directory)

            modified:   exercice_staging.md

    no changes added to commit (use "git add" and/or "git commit -a")

    luc% git add -i exercice_staging.md
    warning: LF will be replaced by CRLF in exercice_staging.md.
    The file will have its original line endings in your working directory.
               staged     unstaged path
      1:    unchanged        +2/-2 exercice_staging.md

    *** Commands ***
      1: status       2: update       3: revert       4: add untracked
      5: patch        6: diff         7: quit         8: help
    What now> p
    warning: LF will be replaced by CRLF in exercice_staging.md.
    The file will have its original line endings in your working directory.
               staged     unstaged path
      1:    unchanged        +2/-2 exercice_staging.md
    Patch update>> 1
               staged     unstaged path
    * 1:    unchanged        +2/-2 exercice_staging.md
    Patch update>>
    warning: LF will be replaced by CRLF in exercice_staging.md.
    The file will have its original line endings in your working directory.
    warning: LF will be replaced by CRLF in exercice_staging.md.
    The file will have its original line endings in your working directory.
    diff --git a/exercice_staging.md b/exercice_staging.md
    index 19fcc4e..0877910 100644
    --- a/exercice_staging.md
    +++ b/exercice_staging.md
    @@ -12,7 +12,7 @@ Cet atelier va porter sur:

     Il ne traitera que ces sujets.

    -PATTERN1
    +Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis sudoribus induratae.


     ## Exercices    
    Stage this hunk [y,n,q,a,d,/,j,J,g,e,?]? y
    @@ -30,5 +30,5 @@ PATTERN1

     ## Todos

    -PATTERN2
    +Proinde concepta rabie saeviore, quam desperatio incendebat et fames, amplificatis viribus ardore incohibili in excidium urbium matris Seleuciae efferebantur, quam comes tuebatur Castricius tresque legiones bellicis sudoribus induratae.

    Stage this hunk [y,n,q,a,d,/,K,g,e,?]? n
    
    *** Commands ***
      1: status       2: update       3: revert       4: add untracked
      5: patch        6: diff         7: quit         8: help
    What now> q
    Bye.

    luc% git commit -m 'commit part 1'
    [sw-exercice-staging 5715f26] commit part 1
     1 file changed, 1 insertion(+), 1 deletion(-)

    luc% git add exercice_staging.md
    warning: LF will be replaced by CRLF in exercice_staging.md.
    The file will have its original line endings in your working directory.

    luc% git commit -m 'commit part 2'
    [sw-exercice-staging warning: LF will be replaced by CRLF in exercice_staging.md.
    The file will have its original line endings in your working directory.
    ca48a4d] commit part 2
    warning: LF will be replaced by CRLF in exercice_staging.md.
    The file will have its original line endings in your working directory.
     1 file changed, 1 insertion(+), 1 deletion(-)
